# MarketPlace Facebook

#### Projeto voltado para a extração de ofertas em grupos do Facebook por meio de Web Crawlers.

## Andamento do Projeto

`ACESSE TODA NOSSA ESTRUTURA CLICANDO NO TÓPICO DESEJADO!!`

- [x] <b>[Definições de Projeto](https://gitlab.com/BDAg/storm-project/wikis/1.1-Defini%C3%A7%C3%B5es-de-Projeto)
- [x] <b>[Documentação da Solução](https://gitlab.com/BDAg/storm-project/wikis/1.2-documenta%C3%A7%C3%A3o-da-solu%C3%A7%C3%A3o)
- [ ] <b>Desenvolvimento da Solução (Sprint 1)
- [ ] <b>Desenvolvimento da Solução (Sprint 2)
- [ ] <b>Desenvolvimento da Solução (Sprint 3)
- [ ] <b>Desenvolvimento da Solução (Sprint 4)
- [ ] <b>Fechamento do Projeto

# Time
- [Diego Henrique Santos Batista](https://gitlab.com/BDAg/marketplace-facebook/wikis/Diego)<br><br>
- [Drielli Laurine Meireles Cândido](https://gitlab.com/BDAg/marketplace-facebook/wikis/Drielli)<br><br>
- [Fabiano José de Carvalho](https://gitlab.com/BDAg/marketplace-facebook/wikis/Fabiano)<br><br>
- [Guilherme Oliveira Paglioni (CTO Aprendiz)](https://gitlab.com/BDAg/marketplace-facebook/wikis/Guilherme)<br><br>
- [Jean Gabriel Da Silva Santos](https://gitlab.com/BDAg/marketplace-facebook/wikis/Jean)<br><br>
- [Jorge Luis Zanguettin (CTO)](https://gitlab.com/BDAg/marketplace-facebook/wikis/Jorge)<br><br>
- [Maria Eduarda De Lima Rosa](https://gitlab.com/BDAg/marketplace-facebook/wikis/Maria-Eduarda)<br><br>
- [Mateus Henrique Marques](https://gitlab.com/BDAg/marketplace-facebook/wikis/Mateus)<br><br>
- [Talita Lima Teodoro](https://gitlab.com/BDAg/marketplace-facebook/wikis/Talita)<br><br>
- [Willy Kevin Correa Da Rosa](https://gitlab.com/BDAg/marketplace-facebook/wikis/Willy)<br><br>
